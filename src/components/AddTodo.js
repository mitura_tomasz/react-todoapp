import React, { Component } from 'react';

class AddTodo extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            title: ''
         }
    }

    onChange = (e) => this.setState({ [e.target.name]: e.target.value });

    onSubmit = (e) => {
        e.preventDefault();
        this.props.addTodo(this.state.title);
        this.setState({ title: '' });
    }

    render() { 
        return ( 
            <form 
                className="add-todo"
                onSubmit={this.onSubmit}
            >
                <input 
                    className="add-todo-input"
                    type="text" 
                    name="title" 
                    placeholder="Add Todo..."
                    style={{ flying: '10', padding: '5px' }}
                    value={this.state.title}
                    onChange={this.onChange}
                />
                <input 
                    className="add-todo-submit"
                    type="submit" 
                    value="Submit"
                    style={{flex: '1'}}
                />
            </form>
         );
    }
}
 
export default AddTodo;