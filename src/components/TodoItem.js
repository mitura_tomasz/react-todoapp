import React, { Component } from 'react';
import PropTypes from 'prop-types';


class TodoItem extends Component {
    state = {  }

    getStyle = () => {
        return {
            background: '#f4f4f4',
            padding: '10px',
            borderBottom: '1px #ccc dotted',
            textAlign: 'left',
            display: 'flex',
            alignItems: 'center',
            textDecoration: this.props.todo.completed ? 'line-through' : 'none'
        }
    }

    render() {
        const { id, title } = this.props.todo;
        return (
            <div style={this.getStyle()}>
                <input 
                    type="checkbox" 
                    defaultChecked={this.props.todo.completed}
                    onChange={this.props.markComplete.bind(this, id)} 
                />
                <p>{ title }</p>
                <button 
                    style={btnStyle}
                    onClick={this.props.delTodo.bind(this, id)}
                >
                    x
                </button>
            </div>
        );
    }
}


TodoItem.propTypes = {
    todo: PropTypes.object.isRequired
}

const btnStyle = {
    background: '#ff0000',
    color: '#fff',
    border: 'none',
    padding: '5px 10px',
    borderRadius: '50%',
    cursor: 'pointer',
    marginLeft: 'auto'
}

export default TodoItem;